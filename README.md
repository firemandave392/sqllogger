# Rust SQL Logging

## What is this for?

This plugin is meant to log activity to a SQL database to make life easier for server administrators.

TODO: Finish updating this README and write documentation.

This plugin is being superseded by version 2. https://gitlab.com/firemandave392/sqllogger_v2 . This version is actively in development and is not ready for use as of December 2023.
